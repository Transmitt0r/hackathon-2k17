/*
	Projection by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
*/

var myThing = "https://dweet.io/get/latest/dweet/for/bics_fdd4e552-e295-4698-9feb-78d4680807bf";
var thing = "bics_fdd4e552-e295-4698-9feb-78d4680807bf";
var SensorStack = [];
var lastDate = 1;

(function($) {

	// Breakpoints.
		skel.breakpoints({
			xlarge:	'(max-width: 1680px)',
			large:	'(max-width: 1280px)',
			medium:	'(max-width: 980px)',
			small:	'(max-width: 736px)',
			xsmall:	'(max-width: 480px)'
		});

	$(function() {

		var	$window = $(window),
			$body = $('body');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			$window.on('load', function() {
				window.setTimeout(function() {
					$body.removeClass('is-loading');
				}, 100);
			});

		// Prioritize "important" elements on medium.
			skel.on('+medium -medium', function() {
				$.prioritize(
					'.important\\28 medium\\29',
					skel.breakpoint('medium').active
				);
			});

	// Off-Canvas Navigation.

		// Navigation Panel.
			$(
				'<div id="navPanel">' +
					$('#nav').html() +
					'<a href="#navPanel" class="close"></a>' +
				'</div>'
			)
				.appendTo($body)
				.panel({
					delay: 500,
					hideOnClick: true,
					hideOnSwipe: true,
					resetScroll: true,
					resetForms: true,
					side: 'left'
				});

		// Fix: Remove transitions on WP<10 (poor/buggy performance).
			if (skel.vars.os == 'wp' && skel.vars.osVersion < 10)
				$('#navPanel')
					.css('transition', 'none');

	});

})(jQuery);


/* our functions*/

var map, heatmap;
var cnt;

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        zoom: 16,
        center: { lat: 48.814118, lng: 9.215092},
        mapTypeId: "satellite"
    });
    
    

    heatmap = new google.maps.visualization.HeatmapLayer({
       data: getPoints(),
       map: map
    });
    heatmap.set('radius', 20);
    addMarker(48.814118, 9.215092, "Humidity");
    
    setInterval(UpdateArray, 3000);
}

/*todo: richtige daten einfügen*/
function getPoints() {
     /*var points = [];
     var pos_lat;
     var pos_lon;
     var humidity;
     var result;
     
     if (SensorStack.length > 0) {
         for (i = 0; i < SensorStack.length - 1; i++) {
             //if (SensorStack.indexOf("{") == 0) {
                 //result = JSON.parse(SensorStack[i]);
                 pos_lat = SensorStack[i].latitude;
                 pos_lon = SensorStack[i].longitude;
                 humidity = SensorStack[i].moisture;
                 points[i] = { location: new google.maps.LatLng(pos_lat, pos_lon), weight: humidity};
             //}
         }
     }
     else {
         points[0] = { location: new google.maps.LatLng(0, 0), weight: 0.5 };
     }
     return points;*/
    
    return [
      { location: new google.maps.LatLng(48.812477, 9.211938), weight: 0.5 },
      { location: new google.maps.LatLng(48.812544, 9.212035), weight: 0.7 },
      { location: new google.maps.LatLng(48.812424, 9.212272), weight: 0.9 },
      { location: new google.maps.LatLng(48.812317, 9.212547), weight: 0.3 },
      { location: new google.maps.LatLng(48.812704, 9.212804), weight: 1.0 },
      { location: new google.maps.LatLng(48.813104, 9.213215), weight: 1.5 },
      { location: new google.maps.LatLng(48.812544, 9.212035), weight: 0.7 },
      { location: new google.maps.LatLng(48.812561, 9.212531), weight: 0.4 },
      { location: new google.maps.LatLng(48.812766, 9.212376), weight: 0.2 },
      { location: new google.maps.LatLng(48.813038, 9.212951), weight: 0.3 },
      { location: new google.maps.LatLng(48.812955, 9.213119), weight: 0.4 },
      { location: new google.maps.LatLng(48.812915, 9.212613), weight: 0.6 },
      { location: new google.maps.LatLng(48.812525, 9.212839), weight: 1.2 },
      { location: new google.maps.LatLng(48.812583, 9.212272), weight: 1.6 },
      { location: new google.maps.LatLng(48.813355, 9.213169), weight: 1.4 },
      { location: new google.maps.LatLng(48.813488, 9.213307), weight: 1.0 },
      { location: new google.maps.LatLng(48.813354, 9.213361), weight: 0.8 },
      { location: new google.maps.LatLng(48.813385, 9.213602), weight: 0.6 },
      { location: new google.maps.LatLng(48.813545, 9.213543), weight: 1.2 },
    ];
 }


    function GetDweet(yourUrl) {
        var Httpreq = new XMLHttpRequest();
        Httpreq.open('GET', yourUrl, false);
        Httpreq.send(null);
        return JSON.parse(Httpreq.responseText);
    }

    function UpdateArray() {
        var dweet = GetDweet(myThing);
        //dweetio.get_latest_dweet_for(thing, function(err, dweet) {
        //var dweet = dweet[0]; // Dweet is always an array of 1
        
        if (dweet.this == "succeeded") {
            
            var dweetContent = dweet.with[0].content;
            if (dweetContent.moisture != null && dweet.with[0].created != lastDate) {
              SensorStack.push(dweetContent);
              console.log("Dweet Number: " + SensorStack.length); // The content of the dweet
            }
            
            if (SensorStack.length > 300) {
                SensorStack.shift();
            }
            if (!heatmap.getMap()) {
                heatmap = new google.maps.visualization.HeatmapLayer({
                    data: getPoints(),
                    map: map
                });
                heatmap.set('radius', 20);
            }
            lastDate = dweet.with[0].created;
        }   
    };


    function addMarker(pos_lat, pos_lon, info) {
        var marker = new google.maps.Marker({
            position: { lat: 48.815305, lng: 9.2127 },
            label: "S",
            map: map
        });
        var infowindow = new google.maps.InfoWindow({
            content: info
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });
    }

    function toggleHeatmap() {
        if (!heatmap.getMap()) {
            heatmap.setMap(map);
            setInterval(UpdateArray, 3000);
        }
        else {
            heatmap.setMap(null);
            clearInterval(UpdateArray);
        }
    }

    function startMeasurement() {
        document.getElementById("measurement1").disabled = true;
        document.getElementById("measurement2").disabled = false;
        // TODO: Change Status to "START"
        dweetio.dweet_for("bics_fdd4e552-e295-4698-9feb-78d4680807bf_status", {
            status: "start"
        }, function (err, dweet) { });
    }

    function stopMeasurement() {
        document.getElementById("measurement2").disabled = true;
        document.getElementById("measurement1").disabled = false;
        // TODO: Change Status to "STOP"
        /*dweetio.dweet_for("bics_fdd4e552-e295-4698-9feb-78d4680807bf_status", {
            status: "stop"
        }, function (err, dweet) { });*/
    }