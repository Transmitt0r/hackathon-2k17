var Tinkerforge = require("tinkerforge");
var dweetClient = require("node-dweetio");
var ROSLIB = require("roslib"); // This is not necessary in the browser

var HOST = 'localhost';
var TINKERPORT = 4223;
var ROSPORT = 9090;
var MUID = 'zU2'; // Change XYZ to the UID of your Moisture Bricklet
var GPSUID = 'qGp';
var SUID = '6qZnJR';
var topic = "bics_fdd4e552-e295-4698-9feb-78d4680807bf";
var readtopic = "bics_fdd4e552-e295-4698-9feb-78d4680807bf_status";

var message = {};

var ros = new ROSLIB.Ros();

ros.on('connection', function() {
  console.log('Connected to websocket server.');
});

ros.on('error', function(error) {
  console.log('Error connecting to websocket server: ', error);
  window.alert('Error connecting to websocket server');
});

ros.on('close', function() {
  console.log('Connection to websocket server closed.');
});

var moveTopic = new ROSLIB.Topic({
  ros: ros,
  name: "/RosAria/cmd_vel",
  messageType: "geometry_msgs/Twist"
});

var dweetio = new dweetClient();
var ipcon = new Tinkerforge.IPConnection(); // Create IP connection
var ipservo = new Tinkerforge.IPConnection();
var m = new Tinkerforge.BrickletMoisture(MUID, ipcon); // Create device object
//var gps = new Tinkerforge.BrickletGPSV2(GPSUID, ipcon);
var servo = new Tinkerforge.BrickServo(SUID, ipservo); // Create device object

ipcon.connect(HOST, TINKERPORT,
  function(error) {
    console.log('Error: 1' + error);
  }
); // Connect to brickd
// Don't use device before ipcon is connected

ipservo.connect(HOST, TINKERPORT,
  function(error) {
    console.log('Error: 1' + error);
  }
); // Connect to brickd
// Don't use device before ipcon is connected

ipcon.on(Tinkerforge.IPConnection.CALLBACK_CONNECTED,
  function(connectReason) {
    ros.connect("ws://" + HOST + ":" + ROSPORT);
  }
);

ipservo.on(Tinkerforge.IPConnection.CALLBACK_CONNECTED,
  function(connectReason) {
    // Set period for moisture value callback to 1s (1000ms)
    // Note: The moisture value callback is only called every second
    //       if the moisture value has changed since the last call!
    console.log(connectReason);
    servo.setOutputVoltage(5000);

    servo.setDegree(0, -10000, 10000);
    servo.setPulseWidth(0, 1000, 2000);
    servo.setPeriod(0, 19500);
    servo.setAcceleration(0, 1000); // Slow acceleration
    servo.setVelocity(0, 65535); // Full speed

    dweetio.listen_for(readtopic, function(dweet) {
      console.log(dweet);
      read();
    });
  }
);

function wait(seconds) {
  var waitTill = new Date(new Date().getTime() + seconds * 1000);
  while(waitTill > new Date()){}
}

var moveForward = function() {
  moveTopic.publish({
    linear: {
      x: 0.3,
      y: 0.0,
      z: 0.0
    },
    angular: {
      x: 0.0,
      y: 0.0,
      z: 0.0
    }
  });

  wait(5);

  moveTopic.publish({
    linear: {
      x: 0.0,
      y: 0.0,
      z: 0.0
    },
    angular: {
      x: 0.0,
      y: 0.0,
      z: 0.0
    }
});
}

var moveBackward = function() {
  moveTopic.publish({
    linear: {
      x: -0.3,
      y: 0.0,
      z: 0.0
    },
    angular: {
      x: 0.0,
      y: 0.0,
      z: 0.0
    }
  });

  wait(5);

  moveTopic.publish({
    linear: {
      x: 0.0,
      y: 0.0,
      z: 0.0
    },
    angular: {
      x: 0.0,
      y: 0.0,
      z: 0.0
    }
});
}

var read = function() {
  moveForward();
  servo.setPosition(0, -7500); // Set to most right position
  servo.enable(0);
  wait(8);
  servo.disable(0);

  m.getMoistureValue(
    function(moist) {
      message['moisture'] = moist;
      //console.log('Moisture Value: ' + moist);

    },
    function(error) {
      console.log('Error: 3' + error);
    }
  );

  message['latitude'] = 48.812477;
  message['longitude'] = 9.211938;

  // gps.getCoordinates(
  //   function(lat, nss, long, eww) {
  //     message['latitude'] = lat;
  //     message['ns'] = nss;
  //     message['longitude'] = long;
  //     message['ew'] = eww;
  //     //console.log('Latitude: ' + lat / 1000000.0 + '° ' + nss);
  //     //console.log('Longitude: ' + long / 1000000.0 + '° ' + eww);
  //   },
  //   function(error) {
  //     console.log('Error: 2' + error);
  //   }
  // );

  wait(5);
  servo.setPosition(0, 10000); // Set to most left position
  servo.enable(0);
  wait(8);
  servo.disable(0);
  moveBackward();
  console.log(message);
  dweetio.dweet_for(topic, message, function(err, dweet) {
    //console.log(dweet); // "my-thing"
  });
}

//setInterval(read, 3000);

console.log('Press key to exit');
process.stdin.on('data',
  function(data) {
    dweetio.stop_listening();
    ipcon.disconnect();
    ipservo.disconnect();
    process.exit(0);
  }
);
